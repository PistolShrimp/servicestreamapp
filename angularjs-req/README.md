# How to run

1. In main directory, run `npm install`
2. Then run `npm start`
3. In a browser, go to `localhost:8000/index.html`

I had issues with the routing since the example project I built this off is set up a little weird, so I decided not to fiddle with it and just use the index.html url.

This page will not run on `localhost:8000`. You need to include the `/index.html`.