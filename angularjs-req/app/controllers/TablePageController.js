'use strict';
(function () {

    /**
     * Main controller. Does everything.
     */
    function TablePageController($scope, $timeout, dataService) {
        const API = 'https://jsonmock.hackerrank.com/api/movies/search/';

        $scope.sortedColumn = null;
        $scope.sortAsc = true;
        $scope.loading = true;
        $scope.error = null;
        $scope.data = null;
        // wrap to work with ng-model
        $scope.model = {
            page: 1,
            filter: null
        };
        var selectedList = [];
        var favouriteList = [];

        // constructor
        function init() {
            getData();
        }

        // gets the data, adjusts page as necessary, sorts based upon
        // current sorting procedure
        function getData() {
            var promise = dataService.getData(API, $scope.model.page, $scope.model.filter);
            promise
                .then((data) => {
                    $scope.data = data;
                    $scope.loading = false;
                    if ($scope.data.total_pages < $scope.model.page) {
                        $scope.model.page = $scope.data.total_pages;
                    }
                    doSort();
                })
                .catch((reason) => {
                    $scope.error = reason;
                    $scope.loading = false;
                })
        }

        // need to update whenever query conditions change
        $scope.$watchGroup(
            ['model.page', 'model.filter'],
            (oldValue, newValue) => {
                $timeout(getData);
            }
        );

        // fires when a row is selected; used in combination with favourite
        // adding.
        $scope.onSelect = (id, selected) => {
            // select
            if (selected && !selectedList.includes(id)) {
                selectedList.push(id);
            }
            // deselect
            else if (!selected && selectedList.includes(id)) {
                selectedList.splice(selectedList.indexOf(id), 1);
            }
        };

        // flip when same column is clicked, otherwise change and reset to asc
        $scope.onSortClick = (column) => {
            if ($scope.sortedColumn === column) {
                $scope.sortAsc = !$scope.sortAsc;
            } else {
                $scope.sortedColumn = column;
                $scope.sortAsc = true;
            }
            doSort();
        };

        // needs to be called whenever whenever data changes
        // and when sort order changes
        function doSort() {
            var result = [...$scope.data.data].sort((a, b) => {
                var order = 0;
                if (a[$scope.sortedColumn] < b[$scope.sortedColumn]) {
                    order = -1;
                } else if (a[$scope.sortedColumn] > b[$scope.sortedColumn]) {
                    order = 1;
                }
                if (!$scope.sortAsc) {
                    order *= -1;
                }
                return order;
            });
            $scope.data.data = result;
        };

        // used to indicate favourite status on table
        $scope.isFavourite = (id) => {
            return favouriteList.includes(id);
        };

        // add to favourite if not already added; prevent dupes
        $scope.addToFavourites = () => {
            selectedList.forEach((value) => {
                if (!favouriteList.includes(value)) {
                    favouriteList.push(value);
                }
            });
        };

        // do constructor
        init();
    }

    const module = angular.module('app');
    module.component('tablePage', {
        templateUrl: '../views/table-page.html',
        controller: [
            '$scope',
            '$timeout',
            'dataService',
            TablePageController
        ],
    });
})();