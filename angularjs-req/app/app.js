'use strict';

const appModule = angular.module('app', [
    'ngRoute',
    'ui.bootstrap'
]);
appModule.config(['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
        $routeProvider
            .otherwise('/');
    }]);
