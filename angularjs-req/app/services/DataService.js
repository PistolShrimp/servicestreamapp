'use strict';

(function () {

    /**
     * Service for collecting data from API.
     */
    function DataService($http, $q) {
        const getData = (api, page, filter) => {
            var defer = $q.defer();
            $http.get(api, {
                params: { page: page, Title: filter }
            })
                .then(
                    function (response) {
                        if (response.status >= 400) {
                            console.error(response.statusText);
                            defer.reject(response.statusText);
                        } else if (response.data) {
                            defer.resolve(response.data);
                        } else {
                            console.error('Data not found');
                            defer.reject('Data not found');
                        }
                    }, function (response) {
                        console.error(response);
                        defer.reject(response);
                    });
            return defer.promise;
        }

        return {
            getData: getData
        };
    }

    const module = angular.module('app');
    module.factory('dataService', ['$http', '$q', DataService]);
})();