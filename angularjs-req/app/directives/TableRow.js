'use strict';

(function () {

    /**
     * A single table row, self contained.
     */
    function TableRow(scope, element, attr) {
        scope.selected = false;
        scope.highlighted = false;

        // constructor, set click handler here to maintain separation of concerns.
        function init() {
            setStyle(scope.selected);
            element.on('click', selectHandler);
        }

        // done here as opposed to in html to maintain separation of concerns.
        function setStyle(select) {
            if (select) {
                element.addClass('selected');
            } else if (element.hasClass('selected')) {
                element.removeClass('selected');
            }
        }

        // on select, set selection state and pass data back to controller
        var selectHandler = (event) => {
            var setSelect = !scope.selected;
            scope.onSelect(scope.data.imdbID, setSelect);
            setStyle(setSelect);
            scope.selected = setSelect;
        };

        // call constructor
        init();
    }

    const module = angular.module('app');
    module.directive('tableRow', function () {
        return {
            restrict: 'A',
            templateUrl: '../views/table-row.html',
            scope: {
                data: '=',
                favourite: '=',
                onSelect: '='
            },
            transclude: true,
            link: TableRow
        }
    });
})();