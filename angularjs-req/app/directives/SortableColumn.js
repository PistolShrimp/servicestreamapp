'use strict';

(function () {

    /**
     * A single column which can be sorted by clicking on it. Logic is handled
     * in callback at the moment.
     * Class is unused for now, but leave here posterity.
     */
    function SortableColumn(scope, element, attr) { }

    const module = angular.module('app');
    module.directive('sortableColumn', function () {
        return {
            restrict: 'A',
            templateUrl: '../views/sortable-column.html',
            scope: {
                sortedColumn: '@',
                sortAsc: '=',
                columnTag: '@',
                callback: '='
            },
            transclude: true,
            link: SortableColumn
        }
    });
})();