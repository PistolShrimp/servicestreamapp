import { useEffect, useState } from 'react';

/**
 * Fetches and returns data.
 * @param {string} api Url of API
 * @param {number} page Page number
 * @param {string} filter Optional title string
 * @returns {[]} [data, setTableData, error]
 */
function useTableData(api, page, filter) {
  const [data, setData] = useState({});
  const [error, setError] = useState(false);

  var fetchData = async () => {
    setError(false);
    var queryString = api + "?page=" + page;
    if (filter) {
      queryString += "&Title=" + filter;
    }

    try {
      const response = await fetch(queryString);
      const data = await response.json();
      // use time to make it clear there's a change
      data.lastUpdate = Date.now();
      setData(data);
    } catch (error) {
      setError(true);
      console.error(error);
    }
  };

  // gets data when query is changed
  useEffect(() => {
    fetchData();
  }, [api, page, filter]);

  // callback passed through hook return for setting data
  var setTableData = (val) => {
    setData(val);
  };

  return [data, setTableData, error];
}

export default useTableData;