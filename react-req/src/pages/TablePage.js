import React, { useState, useEffect } from 'react';
import useTableData from '../hooks/useTableData';
import TableRow from '../components/TableRow';
import Pagination from '../components/Pagination';
import SortableColumn from '../components/SortableColumn';

/**
 * Page and higher order controller.
 */
function TablePage(props) {
  const API = 'https://jsonmock.hackerrank.com/api/movies/search/';

  const [filterText, setFilterText] = useState(null);
  const [loading, setLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const [sortAsc, setSortAsc] = useState(true);
  const [sortColumn, setSortColumn] = useState(null);
  const [data, setData, error] = useTableData(API, currentPage, filterText);
  const [favourites, setFavourites] = useState([]);
  const [selectedList, setSelectedList] = useState([]);

  // cleanup and adjust data after it's collected
  useEffect(() => {
    if (!data || !data.data) {
      return;
    }
    if (currentPage > data.total_pages) {
      setCurrentPage(data.total_pages);
      return; // terminate early and let query run again
    }
    setLoading(false);
    setSelectedList([]);
  }, [data, error]);

  // sort after page changes
  useEffect(() => {
    if (!data || !data.data) {
      return;
    }
    setSorting();
  }, [data.lastUpdate, currentPage]);

  // sort after order is changed
  useEffect(() => {
    if (!data || !data.data) {
      return;
    }
    setSorting();
  }, [sortAsc, sortColumn]);

  // sets data after sorting is performed
  var setSorting = () => {
    var changedArray = doSort(data.data, sortColumn, sortAsc);
    var copy = Object.assign({}, data);
    copy.data = changedArray;
    setData(copy);
  }

  // handle filter change
  var filterChange = (event) => {
    setFilterText(event.target.value);
  }

  // handle page change
  var onPageChange = (page) => {
    setCurrentPage(page);
  };

  // handle clicks on column headers
  var onSortClick = (column) => {
    if (sortColumn === column) {
      setSortAsc(!sortAsc);
    } else {
      setSortColumn(column);
      setSortAsc(true);
    }
  };

  // add to select list upon click
  var onSelect = (id, selected) => {
    var index = selectedList.indexOf(id);
    if (index !== -1 && !selected) {
      var newSelectedList = [...selectedList];
      newSelectedList.splice(index, 1);
    } else if (index === -1 && selected) {
      var newSelectedList = [...selectedList];
      newSelectedList.push(id);
    }
    setSelectedList(newSelectedList);
  };

  // fires when button is clicked
  var favouriteSelected = () => {
    var newFavourites = [...favourites];
    for (var i = 0; i < selectedList.length; i++) {
      if (!favourites.includes(selectedList[i])) {
        newFavourites.push(selectedList[i]);
      }
    }
    setFavourites(newFavourites);
  };

  // check used for rows to mark as favourite
  var isFavourite = (id) => {
    return favourites.indexOf(id) !== -1;
  }

  if (error) {
    return <div>Error...</div>
  }
  if (loading || !data || !data.data) {
    return <div>Loading...</div>;
  }

  return (
    <div id="table-page">
      <form id="filter-form">
        <label htmlFor="filter">
          Filter name:
          <input
            type="text"
            id="filter"
            key="filter"
            name="filter"
            onInput={filterChange} />
        </label>
      </form>

      <table id="table">
        <thead>
          <tr>
            <SortableColumn
              callback={onSortClick}
              columnTag="Title"
              sortedColumn={sortColumn}
              sortAsc={sortAsc} >
              Title
            </SortableColumn>
            <SortableColumn
              callback={onSortClick}
              columnTag="Year"
              sortedColumn={sortColumn}
              sortAsc={sortAsc} >
              Year
            </SortableColumn>
            <SortableColumn
              callback={onSortClick}
              columnTag="imdbID"
              sortedColumn={sortColumn}
              sortAsc={sortAsc} >
              IMDB
            </SortableColumn>
            <th>Favourites</th>
          </tr>
        </thead>
        <tbody>
          {data.data.map((row) => {
            return <TableRow
              data={row}
              favourite={isFavourite(row.imdbID)}
              onSelect={onSelect} />;
          })}
        </tbody>
      </table>
      <Pagination
        totalPages={data.total_pages}
        onChange={onPageChange}
        currentPage={currentPage} />
      <button className="btn" onClick={favouriteSelected}>Favourite!</button>
    </div>
  );
}

function doSort(data, sortColumn, sortAsc) {
  var result = [...data].sort((a, b) => {
    var order = 0;
    if (a[sortColumn] < b[sortColumn]) {
      order = -1;
    } else if (a[sortColumn] > b[sortColumn]) {
      order = 1;
    }
    if (!sortAsc) {
      order *= -1;
    }
    return order;
  });
  return result;
};

export default TablePage;