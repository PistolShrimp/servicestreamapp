/**
 * Catches similar functionality for sortable columns.
 * Sorting is still performed by the table itself.
 * @param {any} props callback, columnTag, sortedColumn, sortAsc
 */
function SortableColumn(props) {
  var sortSymbol = null;
  if (props.sortedColumn === props.columnTag) {
    if (props.sortAsc) {
      sortSymbol = <span>&#x25B2;</span>;
    } else {
      sortSymbol = <span>&#x25BC;</span>;
    }
  }
  return (
    <th onClick={() => props.callback(props.columnTag)}>
      {sortSymbol}
      {props.children}
    </th>
  )
}

export default SortableColumn;