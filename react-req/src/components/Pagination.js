import ReactPaginate from 'react-paginate';

/**
 * Implements a pagination bar for a table or list of results
 * @param {any} props Includes: totalPages, onChange
 */
function Pagination(props) {

  // pass change back to controller to sync data
  var onChange = (event) => {
    props.onChange(event.selected);
  };

  return (
    <div className="pagination-bar">
      <ReactPaginate
        pageCount={props.totalPages}
        onPageChange={onChange}
        breakLabel="..."
        nextLabel=">"
        previousLabel="<"
        pageRangeDisplayed={3}
        marginPagesDisplayed={1}
        renderOnZeroPageCount={null}
      />
    </div>
  );
}

export default Pagination;