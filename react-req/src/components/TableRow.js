import React, { useState, useEffect } from 'react';

/**
 * Displays a single row from raw data.
 * @param {any} props data: { Title, Year, imdbID }, favourite, onSelect(id, bool)
 */
function TableRow(props) {
  const [selected, setSelected] = useState(false);

  // passes selected state back to controller
  var selectHandler = () => {
    props.onSelect(props.data.imdbID, !selected);
    setSelected(!selected);
  };

  // on update, we should always deselect
  useEffect(() => {
    setSelected(false);
  }, [props.data]);

  var highlight = selected ? 'selected' : '';
  var favouritesClassName = props.favourite ? 'favourite' : null;
  var favouriteCol =
    <td className={favouritesClassName}>{props.favourite ? <span>&#9733;</span> : <span>&#9734;</span>}</td>

  return (
    <tr
      key={props.data.imdbID}
      onClick={selectHandler}
      className={highlight}
    >
      <td>{props.data.Title}</td>
      <td>{props.data.Year}</td>
      <td>{props.data.imdbID}</td>
      {favouriteCol}
    </tr>
  );
}

export default TableRow;