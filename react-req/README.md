## Known Issues

The pagination starts at index 0, which means that pages 1 and 2 are the same. This is an issue with the pagination library I decided not to find a workaround for due to time constraints. Aside from that, there should be no major issues to functionality.
